import React from 'react'
import {shallow} from 'enzyme'
import Movies from '../../app/components/Movies'
import MovieInfo from '../../app/components/MovieInfo'

describe('[Components] - Movies', () => {

     test('should check movie exists', () => {

        const node = shallow(<Movies />)
        expect(node.exists()).toEqual(true)
	})

	test('should check MovieInfo(child) component exists', () => {

		const node = shallow(<Movies/>)
    node.setState({movieList : [{
      "imdbID" : 1,
            "Title": "The Last Airbender",
            "Year" : "2010",
            "Type" : "movie",
            "imageUrl" : "app/images/2.jpg"
    }]})
		expect(node.find('MovieInfo').exists()).toEqual(true)
    })
    
   
})