import React from 'react'
import {mount, shallow } from 'enzyme'
import MovieInfo from '../../app/components/MovieInfo'

describe('[Components] - MovieInfo', () => {
const movieCard = {
            "imdbID" : 1,
            "Title": "The Last Airbender",
            "Year" : "2010",
            "Type" : "movie",
            "imageUrl" : "app/images/2.jpg"};
	test('should check movie exists', () => {
        
		const node = mount(<MovieInfo movie={movieCard} />)
        expect(node.prop('movie')).toBeDefined();
        expect(node.prop('movie').Title).toEqual("The Last Airbender");
        expect(node.prop('movie').Year).toEqual("2010");
        expect(node.prop('movie').Type).toEqual("movie");
    })

     it('simulates click events', () => {
        const onButtonClick = jest.fn();
        const mockFunction = jest.fn();
        const wrapper = mount(<MovieInfo movie={movieCard} onMarkAsFav={mockFunction} onButtonClick={onButtonClick} />);
        wrapper.find(`#fav${movieCard._id}`).simulate('click');
        expect(mockFunction).toHaveBeenCalled();
    });
    

})