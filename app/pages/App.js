import React from 'react'
import SamplePage from './Sample'
import Header from '../components/Header'
import Movies from '../components/Movies'

const App = () =>
	<div>
		<Header/>
		<Movies/>
	</div>


export default App
