import React from 'react'

export default class MovieInfo extends React.Component {
	constructor(props) {
        super(props);
         this.state={
             favouriteCount : 0
         }
        this.addToFavourites = this.addToFavourites.bind(this);
    }

    render() {
        const movie = this.props.movie;
       
		return (
            
            <div className="col col-4" key={movie.imdbID} >
                    <div className="card">
                        <img className="movie card-img-top" src={movie.Poster} />
                        <div className="card-body"><div className="col col-12">
                            <a className="movie-title card-title text-truncate" href="">{movie.Title}</a>
                            <div className="movie-year">{movie.Year}</div>
                            <div><button className="btn btn-secondary">{movie.Type}</button>
                            <button id={`fav${movie._id}`} className="btn btn-info" onClick={this.addToFavourites}>
                            {!movie.isFavourite ? (
                                'Add to Favourite'
                             ) : (
                                'Remove from Favourite'
                             )}
                             </button>
                            </div>
                        </div></div>
                    </div>
            </div>
        )
    }

    addToFavourites(){
        this.props.movie['isFavourite'] = this.props.movie['isFavourite'] ? false : true;
        this.props.onMarkAsFav();
    }

    componentDidMount(){
        console.log('mouted..')
    }
}