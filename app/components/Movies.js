import React from 'react'
import MovieInfo from './MovieInfo'
import movieList from '../movielist'


export default class Movies extends React.Component {
	constructor(props) {
        super(props)
       this.state={
           movieList : [],
            favouriteCount : 0
       }
        //this.addToFavourites = this.addToFavourites.bind(this);
        this.getMovies();
	}

	render() {
        const movieList = this.state.movieList;
		return (
			<div>
                <div className="movieContainer container-fluid">
                    <div className="titleMovie"><h3 className="movieHeader">React Redux</h3></div>
                    <div className="row"><div className="col-6">Movies </div><div className="col-6">Favourites : {this.state.favouriteCount}</div></div>
                    <div className="row flex-row flex-nowrap" >
                        {movieList.map(movie => <MovieInfo key={movie.imdbID} movie={movie} onMarkAsFav={this.addToFavourites.bind(this)} />)}
                    </div>
                </div>
            </div>
		)
    }
    
    getMovies(){
        fetch('https://thoughtworksreactreduxmovies.firebaseio.com/movies.json')
        .then(data => data.json())
        .then(res =>{
          //  this.movieList = res;
            this.setState({ movieList: res });
            console.log(this.state.movieList)
        })
        .catch(error => {console.log(error)});
    }

    addToFavourites(){
        console.log(this.state.movieList)
        let count = 0;
        this.state.movieList.map(movie => {
            if(movie.isFavourite){
                count++;
            }
        })
        this.setState({ favouriteCount: count });
         console.log(this.state.favouriteCount)
    }
    componentWillMount(){
         console.log('mounted')
    }
}